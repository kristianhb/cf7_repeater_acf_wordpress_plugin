<?php
/**
 * Plugin Name: AFC – CF7 simple custom repeater
 * Plugin URI: http://www.html24.dk
 * Description: Make a AFC repeater list in contact form 7
 * Version: 1.0
 * Author: Kristian Højlund Bangsø – TwentyFour
 * Author URI: http://www.bangslund.com
 */

// USAGE IN CF7
// [select custom-repeater-list first_as_label repeater:YOUR_REPEATER_NAME repeater-name:YOUR_REPEATER_SUB_FIELD "Select option"]

function dynamic_select_list( $tag ) {


        // Only run on select lists
        if( 'select' !== $tag['type'] && ('select*' !== $tag['type']) ) {
            return $tag;
        } else if ( empty( $tag['options'] ) ) {
            return $tag;
        }

        $term_args = array();

        // Loop thorugh options to look for our custom options
        foreach( $tag['options'] as $option ) {

            $matches = explode( ':', $option );

            if( ! empty( $matches ) ) {

                 if ($matches[0] == 'repeater') {
                    $repeater_field = $matches[1];
                 }

                 if ($matches[0] == 'repeater-name') {
                    $repeater_field_name = $matches[1];
                 }
    
                
            }

        }

        
   
        if( ! empty( $repeater_field ) ) {

            // check if the repeater field has rows of data
            if( have_rows($repeater_field) ):

                // loop through the rows of data
                while ( have_rows($repeater_field) ) : the_row();

                    // display a sub field value
                    $tag['values'][] = get_sub_field($repeater_field_name);

                endwhile;

            endif;
            
        }

    
    return $tag;

}
add_filter( 'wpcf7_form_tag', 'dynamic_select_list', 10 );
