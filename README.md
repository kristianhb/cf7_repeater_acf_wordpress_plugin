# USAGE
Inside of the contact form template you simply insert a this select option and insert your REPEATER_NAME and the field name that's inside of your repeater.

## Example
[select custom-repeater-list first_as_label repeater:YOUR_REPEATER_NAME repeater-name:YOUR_REPEATER_SUB_FIELD "Select option"]


This plugin also allows you to have mulitple repeater lists.